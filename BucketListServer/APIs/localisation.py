from googleplaces import GooglePlaces, types, lang

from credentials import API_KEY

class Localisation():

    def __init__(self, query_name, query_desc=None, query_country_name=None):
        self.api = GooglePlaces(API_KEY)
        self.query_name = query_name
        self.query_desc = query_desc
        self.query_country_name = query_country_name
        self.google_api_results = None


    def get_api_result(self):
        try:
            res = self.api.text_search(query=self.query_name)
            print(res)
            bad_resp_for_name = len(res.places) <= 0

            if bad_resp_for_name:
                if self.query_desc:
                    res = self.api.nearby_search(location=self.query_country_name, keyword=self.query_desc, radius=50000)
                    if len(res.places) > 0:
                        self.google_api_results = res
                    else:
                        return 404
                else:
                    return 404
            else:
                self.google_api_results = res

            return 200
        except Exception as e:
            print(e)
            return 500


loc = Localisation(query_name='Figure height pools', query_country_name='australia')
print(loc.get_api_result())
for p in loc.google_api_results.places:
    print(p)
