from django.conf.urls import url

from . import views

appName = 'indexApp'

urlpatterns = [
    url(r'^$', views.index, name='index'),
]
