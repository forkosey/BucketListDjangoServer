from django.db import models
from django import forms
from django.utils import timezone

class Item(models.Model):
    name = models.CharField("Nom", max_length=300)
    created_date_time = models.DateTimeField("Date de création", default=timezone.now)
    modified_date_time = models.DateTimeField("Date de modofication", default=timezone.now)
    category = models.CharField("Catégorie", max_length=80, default="Catégorie par défaut")
    is_done = models.BooleanField("Check", default=False)
    time_window_start = models.DateField("Début de l'événement", default=None, null=True, blank=True)
    time_window_end = models.DateField("Fin de l'événement", default=None, null=True, blank=True)
    CREATOR_CHOICES = (
        ('MAG', 'Marco-polo'),
        ('SAM', 'Samo-polo')
    )
    creator = models.TextField("Créateur", max_length=3, choices=CREATOR_CHOICES, null=True)

    def __str__(self):
        if len(self.name) > 40:
            return str(self.name)[:40] + '... (' + str(self.modified_date_time) + ')'
        else:
            return str(self.name) + ' (' + str(self.modified_date_time) + ')'


class DateInput(forms.DateInput):
    input_type = 'date'


class ItemForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = ['name', 'category', 'is_done', 'time_window_start', 'time_window_end', 'creator']
        widgets = {
            'name': forms.Textarea(attrs={'cols': 70, 'rows': 2}),
            'time_window_start': DateInput(),
            'time_window_end': DateInput()
        }
