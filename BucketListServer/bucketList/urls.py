from django.conf.urls import url

from . import views

appName = 'bucketList'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^item/(?P<pk>\d+)/$', views.item_detail, name='item_detail'),
    url(r'^new/$', views.new_item, name='new_item'),
    url(r'^categories/$', views.categories, name='categories'),
    url(r'^last_changes/$', views.last_changes, name='last_changes'),
    url(r'^random_item/(?P<category>.+)/$', views.random_item, name='random_item'),
    url(r'^item/(?P<pk>[0-9]+)/edit/$', views.item_edit, name='item_edit'),
    url(r'^item/(?P<pk>[0-9]+)/delete/$', views.item_delete, name='item_delete'),
    url(r'^search/$', views.search_item, name='search_item'),
    url(r'^item_to_come/$', views.item_to_come, name='item_to_come'),
    url(r'^rename_category/$', views.rename_category, name='rename_category'),
]
