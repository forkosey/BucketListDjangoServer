# Generated by Django 2.0.7 on 2018-07-12 14:25

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('bucketList', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='created_date_time',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='item',
            name='modified_date_time',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='item',
            name='time_window_end',
            field=models.DateField(default=None, null=True),
        ),
        migrations.AddField(
            model_name='item',
            name='time_window_start',
            field=models.DateField(default=None, null=True),
        ),
        migrations.AlterField(
            model_name='item',
            name='category',
            field=models.CharField(default='Bucket list par défaut', max_length=80),
        ),
        migrations.AlterField(
            model_name='item',
            name='is_done',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='item',
            name='name',
            field=models.CharField(max_length=300),
        ),
    ]
