# Generated by Django 2.0.7 on 2018-07-12 16:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bucketList', '0003_auto_20180712_1028'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='creator',
            field=models.TextField(choices=[('MAG', 'Marco-polo'), ('SAM', 'Samo-polo')], max_length=3, null=True),
        ),
    ]
