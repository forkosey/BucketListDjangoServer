from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone

import random

from .models import Item, ItemForm

bgs = [
    '26733413434_815fcea204_k.jpg',
    'daniel-von-appen-255535-unsplash.jpg',
    'ethan-elisara-505545-unsplash.jpg',
    'joshua-earle-682748-unsplash.jpg',
    'joseph-barrientos-20264-unsplash.jpg',
    'joschko-hammermann-2224-unsplash.jpg',
    'conner-murphy-345177-unsplash.jpg',
    'brooklyn-morgan-390-unsplash.jpg',
    'lachlan-dempsey-344426-unsplash.jpg',
    'scott-webb-51542-unsplash.jpg',
    'elizeu-dias-520684-unsplash.jpg',
    'leio-mclaren-leiomclaren-302950-unsplash.jpg',
    'kees-streefkerk-352781-unsplash.jpg'
]
session_BG = bgs[random.randint(0, len(bgs))-1]
bg_last_update = timezone.now()

def get_new_bg():
    global bgs
    global session_BG
    global bg_last_update
    if (timezone.now() - bg_last_update).total_seconds() > 3600:
        session_BG = bgs[random.randint(0, len(bgs))-1]
        bg_last_update = timezone.now()


# ============================================ List Requests ============================================ #

def index(request):
    get_new_bg()
    categories = Item.objects.values_list('category', flat=True).distinct()
    ordered_items = []
    for category in categories:
        ordered_items.append({'category': category,
                              'items': list(Item.objects.filter(category=category)[:5])})

    last_modified_mag = Item.objects.filter(creator='MAG').order_by('-created_date_time')[:5]
    last_modified_sam = Item.objects.filter(creator='SAM').order_by('-created_date_time')[:5]
    return render(request, 'bucketList/index.html',
                  {
                      'ordered_items': ordered_items[:5],
                      'last_modified_mag': last_modified_mag,
                      'last_modified_sam': last_modified_sam,
                      'bg': session_BG
                  })


def categories(request):
    get_new_bg()
    categories = Item.objects.values_list('category', flat=True).distinct()
    ordered_items = []
    for category in categories:
        ordered_items.append({'category': category,
                              'items': list(Item.objects.filter(category=category))})
    return render(request, 'bucketList/categories_list.html', {'ordered_items': ordered_items, 'bg': session_BG})


def rename_category(request):
    if 'old_category_name' in request.POST and 'new_category_name' in request.POST and request.POST['old_category_name'] and request.POST['new_category_name']:
        old_name = request.POST['old_category_name']
        new_name = request.POST['new_category_name']
        Item.objects.filter(category=old_name).update(category=new_name)
    return redirect('bucketList:categories')


def last_changes(request):
    get_new_bg()
    last_modified_mag = Item.objects.filter(creator='MAG').order_by('-modified_date_time')[:15]
    last_modified_sam = Item.objects.filter(creator='SAM').order_by('-modified_date_time')[:15]
    return render(request, 'bucketList/last_changes.html',
                  {
                      'last_modified_mag': last_modified_mag,
                      'last_modified_sam': last_modified_sam,
                      'bg': session_BG
                  })


def random_item(request, category):
    get_new_bg()
    items = list(Item.objects.filter(category=category))
    no_time_window = None
    item = None
    while (True):
        item = items[random.randint(0, len(items))-1]
        no_time_window = item.time_window_start == None or item.time_window_end == None
        if (no_time_window or (item.time_window_start <= timezone.now().date() and item.time_window_end >= timezone.now().date())):
            break

    return render(request, 'bucketList/item_detail.html', {'item': item, 'bg': session_BG, 'now': timezone.now().date(), 'start': item.time_window_start, 'end': item.time_window_end, 'random': True})


def search_item(request):
    get_new_bg()
    if 'search_value' in request.POST and request.POST['search_value']:
        search_value = request.POST['search_value']
        items = Item.objects.filter(name__icontains=search_value)
    else:
        items = []
    return render(request, 'bucketList/search_result.html',
                  {
                      'search_result': items,
                      'bg': session_BG
                  })


def item_to_come(request):
    get_new_bg()
    items = Item.objects.filter(time_window_end__gte=timezone.now().date()).order_by('time_window_end')
    return render(request, 'bucketList/item_to_come.html',
                  {
                      'search_result': items,
                      'bg': session_BG
                  })


# ============================================ Item Requests ============================================ #

def new_item(request):
    get_new_bg()
    categories = Item.objects.values_list('category', flat=True).distinct()
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect('bucketList:item_detail', pk=item.pk)
    else:
        form = ItemForm()
    return render(request, 'bucketList/item_edit.html', {'form': form,
                                                         'categories': categories,
                                                         'new': True,
                                                         'bg': session_BG})


def item_edit(request, pk):
    get_new_bg()
    item = get_object_or_404(Item, pk=pk)
    categories = Item.objects.values_list('category', flat=True).distinct()
    if request.method == "POST":
        form = ItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save(commit=False)
            item.modified_date_time = timezone.now()
            item.save()
            return redirect('bucketList:item_detail', pk=item.pk)
    else:
        form = ItemForm(instance=item)
    return render(request, 'bucketList/item_edit.html', {'form': form,
                                                         'categories': categories,
                                                         'new': False,
                                                         'bg': session_BG})


def item_delete(request, pk):
    item = get_object_or_404(Item, pk=pk)
    item.delete()
    return redirect('bucketList:index')


def item_detail(request, pk):
    get_new_bg()
    item = get_object_or_404(Item, pk=pk)
    return render(request, 'bucketList/item_detail.html', {'item': item, 'bg': session_BG, 'now': timezone.now().date(), 'start': item.time_window_start, 'end': item.time_window_end})
