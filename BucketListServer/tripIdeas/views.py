from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from django_countries import countries

import random

from .models import Activity, Trip, ActivityForm, TripForm
from .google_scrapper import get_google_urls

SEASONS_DICT = {'H': 'Hiver',
                'A': 'Automne',
                'P': 'Printemps',
                'E': 'Été'}

bgs = [
    '26733413434_815fcea204_k.jpg',
    'daniel-von-appen-255535-unsplash.jpg',
    'ethan-elisara-505545-unsplash.jpg',
    'joshua-earle-682748-unsplash.jpg',
    'joseph-barrientos-20264-unsplash.jpg',
    'joschko-hammermann-2224-unsplash.jpg',
    'conner-murphy-345177-unsplash.jpg',
    'brooklyn-morgan-390-unsplash.jpg',
    'lachlan-dempsey-344426-unsplash.jpg',
    'scott-webb-51542-unsplash.jpg',
    'elizeu-dias-520684-unsplash.jpg',
    'leio-mclaren-leiomclaren-302950-unsplash.jpg',
    'kees-streefkerk-352781-unsplash.jpg'
]
session_BG = bgs[random.randint(0, len(bgs))-1]
bg_last_update = timezone.now()

def get_new_bg():
    global bgs
    global session_BG
    global bg_last_update
    if (timezone.now() - bg_last_update).total_seconds() > 3600:
        session_BG = bgs[random.randint(0, len(bgs))-1]
        bg_last_update = timezone.now()

def get_country_name_from_code(code):
    return dict(countries)[code]


# ============================================ List Requests ============================================ #

def index(request):
    get_new_bg()
    countries = Activity.objects.values_list('country', flat=True).distinct()[:5]
    ordered_activities = []
    for country in countries:
        ordered_activities.append({
            'country': get_country_name_from_code(country),
            'activities': list(Activity.objects.filter(country=country)[:5])
        })
    return render(request, 'tripIdeas/index.html', {'bg': session_BG, 'ordered_activities': ordered_activities})


def by_country(request):
    get_new_bg()
    countries = Activity.objects.values_list('country', flat=True).distinct()
    ordered_activities = []
    for country in countries:
        ordered_activities.append({
            'filter': get_country_name_from_code(country),
            'activities': list(Activity.objects.filter(country=country))
        })
    return render(request, 'tripIdeas/by_filter.html', {'bg': session_BG, 'ordered_activities': ordered_activities})


def by_season(request):
    get_new_bg()
    seasons = Activity.objects.values_list('season', flat=True).distinct()
    ordered_activities = []
    for season in seasons:
        ordered_activities.append({
            'filter': SEASONS_DICT[season],
            'activities': list(Activity.objects.filter(season=season))
        })
    return render(request, 'tripIdeas/by_filter.html', {'bg': session_BG, 'ordered_activities': ordered_activities})


def last_changes(request):
    get_new_bg()
    creator_list = {}
    mag_trips = Activity.objects.filter(creator='MAG').order_by('-modified_date_time')[:15]
    sam_trips = Activity.objects.filter(creator='SAM').order_by('-modified_date_time')[:15]

    if len(sam_trips) > 0:
        creator_list['Samo-Polo'] = sam_trips
    else:
        creator_list['Samo-Polo'] = None
    if len(mag_trips) > 0:
        creator_list['Marco-Polo'] = mag_trips
    else:
        creator_list['Marco-Polo'] = None
    if len(creator_list) == 0:
        creator_list = None

    return render(request, 'tripIdeas/last_changes.html',
                  {
                      'creator_list': creator_list,
                      'bg': session_BG
                  })


def all_trips(request):
    get_new_bg()
    creator_list = {}
    mag_trips = Trip.objects.filter(creator='MAG').order_by('title')
    sam_trips = Trip.objects.filter(creator='SAM').order_by('title')

    if len(sam_trips) > 0:
        creator_list['Samo-Polo'] = sam_trips
    else:
        creator_list['Samo-Polo'] = None
    if len(mag_trips) > 0:
        creator_list['Marco-Polo'] = mag_trips
    else:
        creator_list['Marco-Polo'] = None
    if len(creator_list) == 0:
        creator_list = None

    return render(request, 'tripIdeas/simple_trip_list.html',
                  {
                      'creator_list': creator_list,
                      'bg': session_BG
                  })


def last_changes_trip(request):
    get_new_bg()
    creator_list = {}
    mag_trips = Trip.objects.filter(creator='MAG').order_by('-modified_date_time')[:15]
    sam_trips = Trip.objects.filter(creator='SAM').order_by('-modified_date_time')[:15]

    if len(sam_trips) > 0:
        creator_list['Samo-Polo'] = sam_trips
    else:
        creator_list['Samo-Polo'] = None
    if len(mag_trips) > 0:
        creator_list['Marco-Polo'] = mag_trips
    else:
        creator_list['Marco-Polo'] = None
    if len(creator_list) == 0:
        creator_list = None

    return render(request, 'tripIdeas/simple_trip_list.html',
                  {
                    'creator_list': creator_list,
                    'bg': session_BG
                  })


def search_item(request):
    get_new_bg()
    items = {}
    if 'search_value' in request.POST and request.POST['search_value']:
        search_value = request.POST['search_value']
        activities = Activity.objects.filter(title__icontains=search_value)
        trips = Trip.objects.filter(title__icontains=search_value)
        items['activities'] = activities
        items['trips'] = trips

    return render(request, 'tripIdeas/search_result.html',
                  {
                      'search_result': items,
                      'bg': session_BG
                  })

# ============================================ Activity Requests ============================================ #

def new_activity(request):
    get_new_bg()
    if request.method == "POST":
        form = ActivityForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect('tripIdeas:activity_detail', pk=item.pk)
    else:
        form = ActivityForm()
    return render(request, 'tripIdeas/activity_edit.html', {'form': form,
                                                             'new': True,
                                                             'bg': session_BG})



def activity_edit(request, pk):
    get_new_bg()
    activity = get_object_or_404(Activity, pk=pk)
    if request.method == "POST":
        form = ActivityForm(request.POST, instance=activity)
        if form.is_valid():
            activity = form.save(commit=False)
            activity.modified_date_time = timezone.now()
            activity.save()
            return redirect('tripIdeas:activity_detail', pk=activity.pk)
    else:
        form = ActivityForm(instance=activity)
    return render(request, 'tripIdeas/activity_edit.html', {'form': form,
                                                            'new': False,
                                                            'bg': session_BG})


def activity_delete(request, pk):
    activity = get_object_or_404(Activity, pk=pk)
    activity.delete()
    return redirect('tripIdeas:index')


def activity_detail(request, pk):
    get_new_bg()
    activity = get_object_or_404(Activity, pk=pk)
    urls = get_google_urls(activity.title)
    return render(request, 'tripIdeas/activity_detail.html', {'activity': activity,
                                                              'bg': session_BG,
                                                              'urls': urls})


# ============================================ Trip Requests ============================================ #

def new_trip(request):
    get_new_bg()
    activities = Activity.objects.all()
    if request.method == "POST":
        form = TripForm(request.POST)
        trips_activities = []
        if form.is_valid():
            if request.POST['activities_pks']:
                activities_pks = request.POST.getlist('activities_pks')
                for pk in activities_pks:
                    trips_activities.append(get_object_or_404(Activity, pk=pk))

            trip = form.save()
            trip.activities.set(trips_activities)
            trip.country_list = [a.country for a in trip.activities.all()]
            trip.save()
            return redirect('tripIdeas:trip_detail', pk=trip.pk)
    else:
        form = TripForm()
    return render(request, 'tripIdeas/trip_edit.html', {'form': form,
                                                        'activities': activities,
                                                        'new': True,
                                                        'bg': session_BG})


def trip_edit(request, pk):
    get_new_bg()
    trip = get_object_or_404(Trip, pk=pk)
    activities = Activity.objects.all()
    activities_tuple = []
    for a in activities:
        activities_tuple.append([a, (a in trip.activities.all())])

    if request.method == "POST":
        form = TripForm(request.POST, instance=trip)
        trips_activities = []
        if form.is_valid():
            if request.POST['activities_pks']:
                activities_pks = request.POST.getlist('activities_pks')
                for pk in activities_pks:
                    trips_activities.append(get_object_or_404(Activity, pk=pk))

            trip = form.save(commit=False)
            trip.activities.set(trips_activities)
            trip.country_list = [a.country for a in trip.activities.all()]
            trip.modified_date_time = timezone.now()
            trip.save()
            return redirect('tripIdeas:trip_detail', pk=trip.pk)
    else:
        form = TripForm(instance=trip)
    return render(request, 'tripIdeas/trip_edit.html', {'form': form,
                                                        'activities': activities_tuple,
                                                        'new': False,
                                                        'bg': session_BG})

def trip_delete(request, pk):
    get_new_bg()
    trip = get_object_or_404(Trip, pk=pk)
    trip.delete()
    return redirect('tripIdeas:index')


def trip_detail(request, pk):
    get_new_bg()
    trip = get_object_or_404(Trip, pk=pk)
    return render(request, 'tripIdeas/trip_detail.html', {'trip': trip, 'bg': session_BG})


def working_on_it(request):
    return render(request, 'working.html')
