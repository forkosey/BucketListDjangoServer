from django.contrib import admin

from .models import Activity, Trip

admin.site.register([Activity, Trip])
