from django.db import models
from django import forms
from django.utils import timezone
from django_countries.fields import CountryField
from django_countries.widgets import CountrySelectWidget

class Activity(models.Model):
    CREATOR_CHOICES = (
        ('MAG', 'Marco-Polo'),
        ('SAM', 'Samo-Polo')
    )
    SEASON_CHOICES = (
        ('E', 'Été'),
        ('A', 'Automne'),
        ('H', 'Hiver'),
        ('P', 'Printemps')
    )
    title = models.CharField("Nom d'activité", max_length=100)
    country = CountryField('Pays')
    created_date_time = models.DateTimeField("Date de création", default=timezone.now)
    modified_date_time = models.DateTimeField("Date de modofication", default=timezone.now)
    creator = models.TextField("Créateur", max_length=3, choices=CREATOR_CHOICES, default='SAM')

    description = models.CharField(max_length=300, null=True, blank=True)
    link = models.CharField('lien', default='', max_length=2083, null=True, blank=True)
    season = models.TextField("Saison", max_length=1, choices=SEASON_CHOICES, null=True, blank=True)
    favorable_date = models.DateField("Date la plus favorable", default=None, null=True, blank=True)
    date_window = models.IntegerField("Plus/Moins (Jour)", default=0)
    postal_code = models.CharField("Code Postal", max_length=15, default=None, null=True, blank=True)


    def __str__(self):
        if len(self.title) > 40:
            return self.title[:40] + '... (' + str(self.country) + ')'
        return self.title + ' (' + str(self.country) + ')'


class Trip(models.Model):
    title = models.CharField("Nom du Voyage", max_length=300)
    description = models.CharField(max_length=500, null=True, blank=True)
    country_list = CountryField(multiple=True)
    created_date_time = models.DateTimeField("Date de création", default=timezone.now)
    modified_date_time = models.DateTimeField("Date de modofication", default=timezone.now)
    activities = models.ManyToManyField(Activity)
    CREATOR_CHOICES = (
        ('MAG', 'Marco-Polo'),
        ('SAM', 'Samo-Polo')
    )
    creator = models.TextField("Créateur", max_length=3, choices=CREATOR_CHOICES, null=True)

    def __str__(self):
        if len(self.title) > 40:
            return self.title[40] + '... (' + str(len(self.country_list)) + ')'
        return self.title + ' (' + str(len(self.country_list)) + ')'


class DateInput(forms.DateInput):
    input_type = 'date'


class ActivityForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = ['title', 'description', 'link', 'country', 'creator', 'season', 'favorable_date', 'date_window', 'postal_code']
        widgets = {
            'title': forms.TextInput(attrs={'size':70, 'autocomplete': 'off'}),
            'description': forms.Textarea(attrs={'cols': 70, 'rows': 3, 'autocomplete': 'off'}),
            'link': forms.TextInput(attrs={'size':70, 'autocomplete': 'off'}),
            'country': CountrySelectWidget(),
            'favorable_date': DateInput(),
            'postal_code': forms.TextInput(attrs={'autocomplete': 'off'}),
        }


class TripForm(forms.ModelForm):
    class Meta:
        model = Trip
        fields = ['title', 'description', 'creator']
        widgets = {
            'title': forms.TextInput(attrs={'size':70, 'autocomplete': 'off'}),
            'description': forms.Textarea(attrs={'cols': 70, 'rows': 3, 'autocomplete': 'off'})
        }
