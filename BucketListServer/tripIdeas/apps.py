from django.apps import AppConfig


class TripideasConfig(AppConfig):
    name = 'tripIdeas'
