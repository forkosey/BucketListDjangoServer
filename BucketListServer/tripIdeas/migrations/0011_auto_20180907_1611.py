# Generated by Django 2.0.7 on 2018-09-07 20:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tripIdeas', '0010_auto_20180907_1311'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activity',
            name='postal_code',
            field=models.CharField(blank=True, default=None, max_length=15, null=True, verbose_name='Code Postal'),
        ),
        migrations.AlterField(
            model_name='activity',
            name='season',
            field=models.TextField(blank=True, choices=[('E', 'Été'), ('A', 'Automne'), ('H', 'Hiver'), ('P', 'Printemps')], max_length=1, null=True, verbose_name='Saison'),
        ),
    ]
