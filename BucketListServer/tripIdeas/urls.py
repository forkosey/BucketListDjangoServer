from django.conf.urls import url

from . import views

appName = 'tripIdeas'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^last_changes/$', views.last_changes, name='last_changes'),
    url(r'^by_country/$', views.by_country, name='by_country'),
    url(r'^by_season/$', views.by_season, name='by_season'),
    url(r'^new_activity/$', views.new_activity, name='new_activity'),
    url(r'^activity/(?P<pk>\d+)/$', views.activity_detail, name='activity_detail'),
    url(r'^activity/(?P<pk>[0-9]+)/edit/$', views.activity_edit, name='activity_edit'),
    url(r'^activity/(?P<pk>[0-9]+)/delete/$', views.activity_delete, name='activity_delete'),
    url(r'^all_trips/$', views.all_trips, name='all_trips'),
    url(r'^last_changes_trip/$', views.last_changes_trip, name='last_changes_trip'),
    url(r'^new_trip/$', views.new_trip, name='new_trip'),
    url(r'^trip/(?P<pk>\d+)/$', views.trip_detail, name='trip_detail'),
    url(r'^trip/(?P<pk>[0-9]+)/edit/$', views.trip_edit, name='trip_edit'),
    url(r'^trip/(?P<pk>[0-9]+)/delete/$', views.trip_delete, name='trip_delete'),
    url(r'^search/$', views.search_item, name='search_item'),
    url(r'^working/$', views.working_on_it, name='working_on_it'),
]
